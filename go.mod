module bitbucket.org/napche/utils

go 1.17

require (
	github.com/golang/protobuf v1.3.1
	github.com/gookit/color v1.2.0
)
