package utils

import (
	"os"
	"testing"
)

func TestColoring(t *testing.T) {
	Successf("This is an %s msg", "success")
	Failf("This is an %s msg", "fail")
	Info("This is an info msg")
	Status("This is an status msg")
	os.Setenv("ENABLE_DEBUG", "TRUE")
	Debugf("Dev will take %d weeks", 4)
}
