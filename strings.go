package utils

import (
	"bytes"
	"log"
	"regexp"
	"strings"
	"unicode"
)

func RemoveComments(text string) string {
	// regular comment: // foobar
	text = ConvertLineEndings(text)
	re := regexp.MustCompile(`(\/\/.*)\n`)
	text = re.ReplaceAllString(text, "\n")

	// Single line comment: /* cmt */
	re = regexp.MustCompile(`(\/\*.*)\*\/`)
	text = re.ReplaceAllString(text, "")

	// Multiple lines comments
	re = regexp.MustCompile(`\/\*[\s+]([\s\S]*?)\*\/`)
	text = re.ReplaceAllString(text, "")

	return text
}

func ConvertLineEndings(text string) string {
	return ReplaceLineEndings(text, "\n")
}

func ReplaceLineEndings(text string, replacement string) string {
	re := regexp.MustCompile(`\x{000D}\x{000A}|[\x{000A}\x{000B}\x{000C}\x{000D}\x{0085}\x{2028}\x{2029}]`)
	return re.ReplaceAllString(text, replacement)
}

func SplitOnLineEndings(text string) []string {
	return strings.Split(text, "\n")
}

func UcFirst(s string) string {
	a := []rune(s)
	a[0] = unicode.ToUpper(a[0])
	return string(a)
}

func ExtractBrackets(text string) [][]string {
	re := regexp.MustCompile(`\(([^()]*|\(([^()]*|\([^()]*\))*\))*\)`)
	return re.FindAllStringSubmatch(text, -1)
}

func GenerateLiteralHash(toHash string) string {
	toHash = strings.ToLower(toHash)
	reg, err := regexp.Compile("[aeiou]+")
	if err != nil {
		log.Fatal(err)
	}
	s := reg.ReplaceAllString(toHash, "")
	var buf bytes.Buffer
	var pc rune
	for i, c := range s {
		if i == 0 {
			pc = c
			buf.WriteRune(c)
		}
		if pc == c {
			continue
		}
		pc = c
		buf.WriteRune(c)
	}

	return strings.ToLower(buf.String())
}


func Comparable(s string) string {
	toHash := strings.ToLower(s)
	reg, err := regexp.Compile("[^a-z0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	return reg.ReplaceAllString(toHash, "")
}

