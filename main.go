package utils

import (
	"fmt"
	"github.com/gookit/color"
	"os"
)

func Success(msg interface{}) {
	color.Green.Println(msg)
}

func Status(msg interface{}) {
	color.Yellow.Println(msg)
}

func Fail(msg interface{}) {
	color.Red.Println(msg)
}

func Info(msg interface{}) {
	color.FgLightBlue.Println(msg)
}

func Debug(msg interface{}) {
	if os.Getenv("ENABLE_DEBUG") == "TRUE" {
		color.Cyan.Println(msg)
	}
}

func Successf(format string, a ...interface{}) {
	Success(fmt.Sprintf(format, a...))
}

func Statusf(format string, a ...interface{}) {
	Status(fmt.Sprintf(format, a...))
}

func Failf(format string, a ...interface{}) {
	Fail(fmt.Sprintf(format, a...))
}

func Infof(format string, a ...interface{}) {
	Info(fmt.Sprintf(format, a...))
}

func Debugf(format string, a ...interface{}) {
	Debug(fmt.Sprintf(format, a...))
}

func SuccessStr(format string, a ...interface{}) string{
	return color.Green.Sprintf(format, a...)
}

func StatusStr(format string, a ...interface{}) string{
	return color.Yellow.Sprintf(format, a...)
}

func InfoStr(format string, a ...interface{}) string{
	return color.FgLightBlue.Sprintf(format, a...)
}

func FailStr(format string, a ...interface{}) string {
	return color.Red.Sprintf(format, a...)
}

func DebugStr(format string, a ...interface{}) string {
	return color.Cyan.Sprintf(format, a...)
}
