package utils

import (
	"time"
	"github.com/golang/protobuf/ptypes/timestamp"
)

func CurrentTimeStamp() *timestamp.Timestamp {
	return &timestamp.Timestamp{Seconds: time.Now().Unix()}
}
